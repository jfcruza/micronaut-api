import ch.qos.logback.classic.Level
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import ch.qos.logback.core.status.OnErrorConsoleStatusListener
import ch.qos.logback.core.util.FileSize

enum Environment{
  production(Level.INFO), development(Level.DEBUG)
  private final Level level
  Environment(Level level) {this.level=level}
  Level getLevel() {this.level}
}

statusListener(OnErrorConsoleStatusListener)

def appenderList = ["CONSOLE","ROLLING"]
String appName = "micronaut-api"
String path = System.getProperty("logging.root") ?: "${System.getProperty('user.home')}/.project_config/logs/$appName"
Environment environment = (System.getProperty("environment") ?: "development") as Environment
println("This is prod log config")
println(environment)
println(environment.level)

scan("30 seconds")
appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%cyan(%d{HH:mm:ss.SSS}) %green([%thread]) %highlight(%-5level) %magenta(%logger{15}) - %msg %n"
  }
}

appender("ROLLING", RollingFileAppender) {

  encoder(PatternLayoutEncoder) {
    pattern = "%cyan(%d{HH:mm:ss.SSS}) %green([%thread]) %highlight(%-5level) %magenta(%logger{15}) - %msg %n"
  }
  rollingPolicy(TimeBasedRollingPolicy){
    FileNamePattern = "${path}/micronaut-api-%d{yyyy-MM-dd}.log"
    maxHistory = 10
    totalSizeCap = FileSize.valueOf("5GB")
  }

}

logger "team.development", environment.level, appenderList