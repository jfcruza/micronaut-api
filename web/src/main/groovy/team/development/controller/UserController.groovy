package team.development.controller

import groovy.util.logging.Slf4j
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.Put
import io.micronaut.validation.Validated
import team.development.command.UserSave
import team.development.command.UserUpdate
import team.development.domain.User
import team.development.repository.SortingAndOrderArguments
import team.development.repository.UserRepository
import team.development.service.UserService

import javax.validation.Valid

@Slf4j
@Validated
@Controller("/users")
class UserController {

  UserService userService

  UserRepository userRepository

  UserController( UserService userService, UserRepository userRepository ) {
    this.userService = userService
    this.userRepository = userRepository
  }

  @Get("/{id:[1-9]+}")
  @Produces(MediaType.APPLICATION_JSON)
  User getUser(Long id) {
    log.info("User GET id: $id")
    userRepository.findById(id).orElse(null)
  }

  @Put("/")
  public HttpResponse update(@Body @Valid UserUpdate command) {
    log.info("${command.dump()}")
    int numberOfEntitiesUpdated = userRepository.update(command.toUser())
    HttpResponse
      .noContent()
      .header(HttpHeaders.LOCATION, location(command.id).path)
  }

  @Get(value = "/list{?args*}")
  public List<User> list(@Valid SortingAndOrderArguments args) {
    userRepository.findAll(args)
  }

  @Post("/")
  public HttpResponse<User> save(@Body @Valid UserSave cmd) {
    User user = userRepository.save(cmd.toUser())
    log.info "$user"
    HttpResponse
      .created(user)
      .headers{headers -> headers.location(location(user.id))}
  }

  @Delete("/{id}")
  public HttpResponse delete(Long id) {
    userRepository.deleteById(id)
    return HttpResponse.noContent()
  }

  protected URI location(Long id) {
    URI.create("/users/" + id)
  }

  protected URI location(User user) {
    location(user.id)
  }

}
