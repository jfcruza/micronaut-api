package team.development.unit.controller

import groovy.util.logging.Slf4j
import io.micronaut.context.ApplicationContext
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.reactivex.Flowable
import org.junit.FixMethodOrder
import org.junit.runners.MethodSorters
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import team.development.domain.User

import java.lang.Void as Should
import static org.spockframework.util.Assert.*

@Slf4j
@Unroll
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserControllerSpec extends Specification{

  @Shared
  @AutoCleanup
  EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)

  @Shared
  @AutoCleanup
  RxHttpClient client = embeddedServer.applicationContext.createBean(RxHttpClient, embeddedServer.getURL())

  Should "Spec 1 GET user by id #_id"() {
    given: "a user id"
      Long id = _id
    when: "the uri is called"
      log.debug("Calling /library/api/user/$id")
      HttpRequest request = HttpRequest.GET("/library/api/user/$id")
      HttpResponse<User> response = client.toBlocking().exchange(request, Argument.of(User))
    then: "check if the endpoint answers correctly"
      log.debug("Response status ${response?.status()}")
      response.status() == HttpStatus.OK
      response.body()
    when: "the response body is requested"
      User user = response.body()
    then: "the user must have its properties correctly assigned"
      log.debug("${user.dump()}")
      user.id
      user.userName
    where:
      _id << [1L, 2L]
  }

}
