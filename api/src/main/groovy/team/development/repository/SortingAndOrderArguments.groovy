package team.development.repository

import javax.annotation.Nullable
import javax.validation.constraints.Pattern
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

class SortingAndOrderArguments {

  @Nullable
  @PositiveOrZero
  Integer offset;

  @Nullable
  @Positive
  Integer max;

  @Nullable
  @Pattern(regexp = "id|userName|lastName|firstName|mail|age")
  String sort;

  @Pattern(regexp = "asc|ASC|desc|DESC")
  @Nullable
  String order;

}
