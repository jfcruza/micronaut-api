package team.development.repository

import team.development.domain.User

import javax.validation.constraints.NotNull

interface UserRepository {

  Optional<User> findById( @NotNull Long id )

  User save( @NotNull User user )

  void deleteById( @NotNull Long id )

  List<User> findAll()
  List<User> findAll( SortingAndOrderArguments args )

  Integer update( @NotNull User user )

}
