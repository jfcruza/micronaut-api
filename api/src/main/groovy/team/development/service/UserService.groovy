package team.development.service

import team.development.domain.User

interface UserService {
  User findOne( Long id )
}