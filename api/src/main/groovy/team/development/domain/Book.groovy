package team.development.domain

class Book {

  String title
  String author
  Integer year
  Integer edition
  String editorial
  String country

}
