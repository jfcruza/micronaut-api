package team.development.domain

import com.sun.istack.NotNull

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "user")
class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id
  @NotNull
  @Column(name = "username", nullable = false, unique = true)
  String userName
  @Column(name = "first_name")
  String firstName
  @Column(name = "last_name")
  String lastName
  @Column(name = "mothers_last_name")
  String mothersLastName
  @Column(name = "mail")
  String mail
  @Column(name = "phone")
  String phone
  @Column(name = "age")
  Integer age

  @Override
  String toString() {
    "${User} [" +
      "id: $id, " +
      "userName: $userName, " +
      "name: $firstName $lastName $mothersLastName, " +
      "mail: $mail, " +
      "phone: $phone, " +
      "age: $age]"
  }
}
