package team.development.command

import team.development.domain.User

import javax.validation.constraints.NotBlank

class UserSave {

  @NotBlank
  String userName
  String firstName
  String lastName
  String mothersLastName
  String mail
  String phone
  Integer age

  User toUser() {
    new User(
      userName: userName,
      firstName: firstName,
      lastName: lastName,
      mothersLastName: mothersLastName,
      mail: mail,
      phone: phone,
      age: age
    )
  }

}

