package team.development.command

import team.development.domain.User

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class UserUpdate {

  @NotNull
  Long id
  @NotBlank
  String userName
  String firstName
  String lastName
  String mothersLastName
  String mail
  String phone
  Integer age

  User toUser() {
    new User(
      id: id,
      userName: userName,
      firstName: firstName,
      lastName: lastName,
      mothersLastName: mothersLastName,
      mail: mail,
      phone: phone,
      age: age
    )
  }

}
