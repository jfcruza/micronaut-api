package team.development.unit.service

import groovy.util.logging.Slf4j
import org.junit.FixMethodOrder
import org.junit.runners.MethodSorters
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import team.development.domain.User
import team.development.service.UserService
import team.development.service.impl.UserServiceImpl

import java.lang.Void as Should
import static org.spockframework.util.Assert.*

@Slf4j
@Unroll
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserServiceSpec extends Specification {

  @Shared
  UserService service = new UserServiceImpl()

  Should "Spec 1 find a user by id #_id"() {
    given: "a user id"
      Long id = _id
    when: "user is queried"
      User user = service.findOne id
    then: "the user must have it's properties properly setted"
      log.debug("${user.dump()}")
      user.id
    where:
      _id << [1L, 2L]
  }

}
