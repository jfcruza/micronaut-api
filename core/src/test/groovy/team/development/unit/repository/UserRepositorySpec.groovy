package team.development.unit.repository

import groovy.util.logging.Slf4j
import io.micronaut.context.ApplicationContext
import org.junit.FixMethodOrder
import org.junit.runners.MethodSorters
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import team.development.domain.User
import team.development.repository.UserRepository
import team.development.repository.impl.UserRepositoryImpl

import javax.persistence.EntityManager
import javax.persistence.Query
import javax.persistence.TypedQuery
import java.lang.Void as Should

@Unroll
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserRepositorySpec extends Specification {

  @Shared
  UserRepositoryImpl repository = new UserRepositoryImpl()
  
  Should "Spec 1 get modified properties from two Users for update"() {
    given: "an old user"
      User oldUser = _old
    and: "a modified user"
      User modUser = _mod
    when: "both user properties are compared"
      List<String> modProps = repository.compareUserProps modUser, oldUser
    then: "the modified properties must be the expected ones"
      modProps == _modProps
    where:
      _old << [new User(id: 1, userName: "jfcruz", firstName: "jose", lastName: "cruz", mothersLastName: "arroyo", mail: "j@mail.com", phone: "5555-5555", age: 24),
               new User(id: 2, userName: "centauro", firstName: "Gil", lastName: "el centauro", mothersLastName: "paps", mail: "el_centauro@el.centauro", phone: "666", age: 999),
               new User(id: 2, userName: "centauro", firstName: "Gil", lastName: "el centauro", mothersLastName: "paps", mail: "el_centauro@el.centauro", phone: "666", age: 999)
              ]
      _mod << [new User(id: 1, userName: "jfcruz001", firstName: "jose", lastName: "cruz", mothersLastName: "arroyo", mail: "j@mail.com", phone: "5555-5555", age: 24),
               new User(id: 2, userName: "centauro", firstName: "Gil", lastName: "el centauro", mothersLastName: "paps", mail: "el_centauro@el.centauro", phone: "666", age: 999),
               new User(id: 2, userName: "asd", firstName: "asd", lastName: "el asd", mothersLastName: "asd", mail: "asd@el.centauro", phone: "asd", age: 22)
              ]
      _modProps << [["userName"],[],["userName", "firstName", "lastName", "mothersLastName", "mail", "phone", "age"]]
  }

  Should "Spec 2 assemble update props query"() {
    given: "a list of modified properties of a user"
      List<String> modProps = _modProps
    when: "the SET part of query is assembled"
      String setProps = repository.assembleSetProps modProps
    then: "the string must be as expected"
      setProps == _expectedSetProps
    where:
      _modProps << [["userName"], [], ["userName", "firstName", "lastName", "mothersLastName", "mail", "phone", "age"]]
      _expectedSetProps << ["userName = :userName",
                            "",
                            "userName = :userName, firstName = :firstName, lastName = :lastName, mothersLastName = :mothersLastName, mail = :mail, phone = :phone, age = :age"]
  }

  Should "Spec 3 assemble whole update query for user"() {
    given: "a list of modified props"
      List<String> modProps = _modProps
    when: "update query is assembled"
      String updateQuery = repository.assembleUpdateQuery modProps
    then: "the query must be as expected"
      updateQuery == _expectedQuery
    where:
      _modProps << [["userName"], [], ["userName", "firstName", "lastName", "mothersLastName", "mail", "phone", "age"]]
      _expectedQuery << [
        "UPDATE User u SET userName = :userName WHERE id = :id",
        "UPDATE User u SET  WHERE id = :id",
        "UPDATE User u SET userName = :userName, firstName = :firstName, lastName = :lastName, mothersLastName = :mothersLastName, mail = :mail, phone = :phone, age = :age WHERE id = :id"
      ]
  }

  @Ignore
  Should "Spec 4 set parameters to update query"() {
    given: "a list of modified parameters"
      List<String> modProps = _modProps
    and: "an update query string"
      String updateQuery = _query
    and: "a modified user"
      User user = _user
    when: "actual Query object is generated"
      Query query = repository.buildUpdateQuery modProps, updateQuery, user
    then: "the query parameters must be as expected"
      println "${query.parameters}"
      query.parameters
    where:
      _modProps << [["userName"], [], ["userName", "firstName", "lastName", "mothersLastName", "mail", "phone", "age"]]
      _query << [
        "UPDATE User u SET userName = :userName WHERE id = :id",
        "UPDATE User u SET  WHERE id = :id",
        "UPDATE User u SET userName = :userName, firstName = :firstName, lastName = :lastName, mothersLastName = :mothersLastName, mail = :mail, phone = :phone, age = :age WHERE id = :id"
      ]
      _user << [
        new User(id: 1, userName: "jfcruz001", firstName: "jose", lastName: "cruz", mothersLastName: "arroyo", mail: "j@mail.com", phone: "5555-5555", age: 24),
        new User(id: 2, userName: "centauro", firstName: "Gil", lastName: "el centauro", mothersLastName: "paps", mail: "el_centauro@el.centauro", phone: "666", age: 999),
        new User(id: 2, userName: "asd", firstName: "asd", lastName: "el asd", mothersLastName: "asd", mail: "asd@el.centauro", phone: "asd", age: 22)
      ]
  }

}
