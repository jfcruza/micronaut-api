statusListener(OnErrorConsoleStatusListener)

def appenderList = ["CONSOLE"]

scan("30 seconds")
appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%cyan(%d{HH:mm:ss.SSS}) %green([%thread]) %highlight(%-5level) %magenta(%logger{15}) - %msg %n"
  }
}

logger "team.development", DEBUG, appenderList