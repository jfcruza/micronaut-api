package team.development.repository.impl

import groovy.util.logging.Slf4j
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession
import io.micronaut.spring.tx.annotation.Transactional
import team.development.config.ApplicationConfiguration
import team.development.domain.User
import team.development.repository.SortingAndOrderArguments
import team.development.repository.UserRepository
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query
import javax.persistence.TypedQuery
import javax.validation.constraints.NotNull

@Slf4j
@Singleton
class UserRepositoryImpl implements UserRepository {

  private final static List<String> VALID_PROPERTY_NAMES =
    ["id", "userName", "firstName", "lastName", "mothersLastName", "mail", "phone", "age"]
  private final static String INIT_UP = "UPDATE User u SET "
  private final static String END_UP = " WHERE id = :id"

  @PersistenceContext
  private EntityManager entityManager
  private final ApplicationConfiguration applicationConfiguration

  UserRepositoryImpl(@CurrentSession EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
    this.entityManager = entityManager
    this.applicationConfiguration = applicationConfiguration
  }

  UserRepositoryImpl(){}

  @Override
  @Transactional( readOnly = true )
  Optional<User> findById(@NotNull Long id) {
    Optional.ofNullable entityManager.find(User, id)
  }

  @Override
  @Transactional
  User save(@NotNull User user) {
    entityManager.persist user
    user
  }

  @Override
  @Transactional
  void deleteById(@NotNull Long id) {
    findById(id).ifPresent{ user -> entityManager.remove(user) }
  }

  @Override
  @Transactional(readOnly = true)
  List<User> findAll() {
    entityManager.createQuery("SELECT u FROM User as u", User).resultList
  }

  @Override
  @Transactional(readOnly = true)
  List<User> findAll(SortingAndOrderArguments args) {
    String qlString = "SELECT u FROM User as u"
    if (args.order && args.sort && VALID_PROPERTY_NAMES.contains(args.sort))
      qlString += " ORDER BY u." + args.sort + " " + args.sort.toLowerCase()
    TypedQuery<User> query = entityManager.createQuery(qlString, User)
    query.maxResults = args.max ?: applicationConfiguration.max
    args.offset?.with { query.firstResult = it }
    query.resultList
  }

  @Override
  @Transactional
  Integer update(@NotNull User user) {
    log.info("UPDATE")
    findById(user.id)?.get()?.with { oldUser ->
      log.info("User found ${user.dump()}")
      compareUserProps(user, oldUser)?.with { modProps ->
        buildUpdateQuery(modProps, assembleUpdateQuery(modProps), user).executeUpdate()
      }
    }
  }

  List<String> compareUserProps(User original, User modified){
    VALID_PROPERTY_NAMES.inject([]) {modProps, property ->
      if( modified.("$property") != original.("$property") )
        modProps << property
      modProps
    }
  }

  String assembleSetProps(List<String> modProps) {
    modProps.inject([]) { setQuery, prop ->
      setQuery << "$prop = :$prop"
      setQuery
    }.join(', ')
  }

  String assembleUpdateQuery(List<String> modProps) {
    "$INIT_UP${assembleSetProps(modProps)}$END_UP"
  }

  Query buildUpdateQuery(List<String> modProps, String updateQuery, User user) {
    log.info("Modified props: ${modProps}")
    log.info("Query $updateQuery")
    entityManager.createQuery(updateQuery).with { query ->
      modProps.each { prop ->
        query.setParameter(prop, user.("$prop"))
      }
      query.setParameter("id", user.id)
      query
    }
  }

}
