package team.development.service.impl

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import team.development.domain.User
import team.development.service.UserService

import javax.inject.Singleton

@Slf4j
@Singleton
@CompileStatic
class UserServiceImpl implements UserService {

  @Override
  User findOne(Long id) {
    log.info("Querying user with id: $id")
    new User(
      id: 1,
      userName: "j.cruz",
      firstName: "José",
      lastName: "Cruz",
      mothersLastName: "Arroyo",
      mail: "j.cruz@mail.com",
      phone: "(55)5412-4567",
      age: 24
    )
  }
}
