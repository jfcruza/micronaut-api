package team.development.config

import javax.validation.constraints.NotNull

interface ApplicationConfiguration {

  @NotNull Integer getMax()

}