package team.development.config

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("application")
class ApplicationConfigurationProperties implements ApplicationConfiguration{

  protected final Integer DEFAULT_MAX = 10
  Integer max = DEFAULT_MAX

  void setMax(Integer max) { if (max) this.max = max }

}
